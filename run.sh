#!/bin/bash

for file in /net/corpora/twitter2/Tweets/2010/12/20101231:*; do
	run=`zless $file | /net/corpora/twitter2/tools/tweet2tab -i user user.name | python3 find_usernames.py`
	echo $file
	echo $run
done;

for file in /net/corpora/twitter2/Tweets/2016/12/20161231:*; do
	run=`zless $file | /net/corpora/twitter2/tools/tweet2tab -i user user.name | python3 find_usernames.py`
	echo $file
	echo $run
done;

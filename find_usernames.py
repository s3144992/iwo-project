#!/usr/bin/python3

import sys

def find_username(line, count, count1):
	names = line.split('\t')
	tab = "\t"
	username = names[0]
	if tab in line:	
		normal_name = names[1:]
	if ' ' in str(normal_name):
		normal_name = normal_name[0].split()
		special_count = 0
		for item in normal_name:
			if item.lower() in username.lower() and len(item) > 3:
				special_count = 1
		if special_count == 1:
			count = count + 1
	else:
		normal_name = normal_name[0]
		normal_name = normal_name.rstrip()
		if normal_name.lower() in username.lower():
			count = count + 1
	count1 = count1 + 1
	return (count, count1)
	

def main():
	count = 0
	count1 = 0
	tuple = (count, count1)
	for line in sys.stdin:
		tuple = find_username(line, *tuple)
	print(tuple)

if __name__ == '__main__':
	main()

#!/bin/bash

echo 'The amount of tweets on March 1st 2017 at 12:00:'
#Uses 'zless' to access the files in the directory of March 1st 2017 at 12:00, uses the 'tweet2tab' tool to show the text from the tweets, uses '-i' to ignore errors, 
#and uses 'wc -l' to count the number of lines.
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | wc -l

echo 'The amount of unique tweets on March 1st 2017 at 12:00:'
#Uses 'zless' to access the files in the directory of March 1st 2017 at 12:00, uses the 'tweet2tab' tool to show the text from the tweets, uses '-i' to ignore errors, 
#uses 'awk' to omit duplicate tweets and uses 'wc -l' to count the number of lines.
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | awk '!x[$0]++' |  wc -l

echo 'The amount of retweets out of the unique tweets on March 1st 2017 at 12:00:'
#Uses 'zless' to access the files in the directory of March 1st 2017 at 12:00, uses the 'tweet2tab' tool to show the text from the tweets, uses '-i' to ignore errors,
#uses 'awk' to omit duplicate tweets, uses 'grep' to find retweets and uses 'wc -l' to count the number of lines.
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | awk '!x[$0]++' | grep '^RT @' | wc -l

echo 'The first 20 unique tweets that are not retweets on March 1st 2017 at 12:00:'
#Uses 'zless' to access the files in the directory of March 1st 2017 at 12:00, uses the 'tweet2tab' tool to show the text from the tweets, uses '-i' to ignore errors,
#uses 'awk' to omit duplicate tweets, uses 'grep -v' to show only tweets that are not retweets and uses 'head -20' to show only the first 20 lines.
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | awk '!x[$0]++' | grep -v '^RT @' | head -n20
